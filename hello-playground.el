;;; hello-playground.el --- Create playground for multi language to say (more than) hello world  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  teddy

;; Author: teddy <mlc880926@gmail.com>
;; Maintainer: teddy <mlc880926@gmail.com>
;; URL: https://gitlab.com/emacs-weekly/hello-playground
;; Version: 1.0.0
;; Package-Requires: ((emacs "29.1") (f "0.20.0"))
;; Keywords: tool playground

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Create playground for multi language to say (more than) hello world
;;

;;; Code:

;; Happy coding! ;)


(require 'f)
(require 'cl-macs)

(defun hello-playground--default-env-path ()
  "demo"
  (f-expand "env"
            (cond (load-file-name (file-name-directory load-file-name))
                  ((locate-library "hello-playground") (file-name-directory (locate-library "hello-playground")))
                  (t default-directory))))

(defcustom hello-playground-env-dir
  (hello-playground--default-env-path)
  "The directory where hello playground language template files live"
  :type 'directory
  :group 'hello-playground)

(defcustom hello-playground-dir "~/hello-playground"
  "The directory where hello playground generate source code"
  :type 'directory
  :group 'hello-playground)

;;;###autoload
(defun hello-playground ()
  "run hello playground and select a language"
  (interactive)
  (let* ((lang (completing-read "please select language: " (hello-playground--list-languages)))
	 (config-values (hello-playground--get-executor lang))
	 (target-dir (hello-playground--create-playground lang)))

    (let ((default-directory target-dir))
      (cl-multiple-value-bind (executor sourceFile) config-values
	(find-file-other-window (f-join target-dir sourceFile))
	(compile (format "%s %s" executor sourceFile))))))

;;;###autoload
(defun hello-playground-exec ()
  "run current playground"
  (interactive)
  (let ((default-directory (hello-playground--get-project-current)))
    (seq-let (executor sourceFile packageFile) (hello-playground--get-config-data default-directory)
      (compile (format "%s %s" executor sourceFile)))))

;;;###autoload
(defun hello-playground-switch-between-package-and-main ()
  "switch between current playground package file and main file"
  (interactive)
  (let ((playground-dir (hello-playground--get-project-current)))
    (seq-let (executor sourceFile packageFile) (hello-playground--get-config-data playground-dir)
      (if (equal packageFile (f-filename buffer-file-name))
	    (find-file (f-join playground-dir sourceFile))
          (find-file (f-join playground-dir packageFile))))))

;;;###autoload
(defun hello-playground-list-playgrounds ()
  "select playground for given lang"
  (interactive)
  (let* ((lang (completing-read "please select language: " (hello-playground--list-languages)))
         (items (hello-playground--list-playground-items lang)))
    (if (not items)
        (message "No playground in %s" lang)
      (progn
        (let* ((playground-name (completing-read "please select playground: " (hello-playground--list-playground-items lang)))
  	     (playground-config (json-read-file (f-join (f-expand hello-playground-dir) lang playground-name "hello-playground.json"))))
  	(let-alist playground-config
  	  (find-file (f-join (f-expand hello-playground-dir) lang playground-name .sourceFile))))))))

;;;###autoload
(defun hello-playground-rename-playgrounds ()
  "rename current playground"
  (interactive)
  (hello-playground--with-playground
   (let* ((current-directory (hello-playground--get-project-current))
	  (new-name (read-string (concat "new name (" (f-filename current-directory) "):")))
	  (new-directory (f-join (f-parent current-directory) new-name)))
     (if (f-directory-p new-directory)
         (message "playground %s already exists" new-directory)
       (progn
	 (dired-rename-file (hello-playground--get-project-current) new-directory t)
	 (message "Rename playground from %s to %s" (f-filename current-directory) new-name))))))

;;;###autoload
(defun hello-playground-delete-playgrounds ()
  "delete current playground"
  (interactive)
  (hello-playground--with-playground
   (let ((current-directory (hello-playground--get-project-current)))
     (when (y-or-n-p (concat "Do you want to delete current playground (" current-directory ")" ))
       (dired-delete-file current-directory 'always)
       (dolist (buffer (buffer-list))
  	 (with-current-buffer buffer
  	   (when (and buffer-file-name
  		      (f-parent-of? current-directory buffer-file-name))
  	     (kill-buffer buffer)))))
     (message "Playground and related buffer deleted"))))

(defun hello-playground--list-languages ()
  (delete-dups (append (mapcar #'car (hello-playground--get-lang-from-custom)) 
                       (mapcar #'car (hello-playground--get-lang-from-default)))))

(defun hello-playground--create-playground (lang)
  (let ((target-dir (f-join (f-expand hello-playground-dir) lang (hello-playground--timestamp)))
        (source-dir (hello-playground--get-env-path lang)))
    (f-mkdir-full-path (f-parent target-dir))
    (hello-playground--copy-directory source-dir target-dir)
    target-dir))

(defun hello-playground--copy-directory (source-dir target-dir)
  "Recursively copy SOURCE-DIR to TARGET-DIR, replacing symlinks with real files."
  (unless (file-directory-p target-dir)
    (make-directory target-dir t))
  (dolist (file (directory-files source-dir t))
    (unless (or (string-suffix-p "." file)
                (string-suffix-p ".." file))
      (let ((target-file (expand-file-name (file-name-nondirectory file) target-dir)))
        (if (file-directory-p file)
            (hello-playground--copy-directory file target-file)
          (if (file-symlink-p file)
              (progn
                (setq real-file (file-truename file))
                (copy-file real-file target-file t t))
            (copy-file file target-file t t)))))))

(defun hello-playground--get-executor (lang)
  (let* ((config-file (f-join (hello-playground--get-env-path lang) "hello-playground.json"))
         (config-content (json-read-file config-file)))
    (let-alist config-content
      (list .executor .sourceFile))))

(defun hello-playground--timestamp()
  (format-time-string "%y-%02m-%02d-%02H%02M%02S" (current-time)))

(defun hello-playground--get-env-path (lang)
  (or (cdr (assoc lang (hello-playground--get-lang-from-custom)))
      (cdr (assoc lang (hello-playground--get-lang-from-default)))))

(defun hello-playground--custom-env-exists ()
  (equal (symbol-value 'hello-playground-env-dir) (hello-playground--default-env-path)))

(defun hello-playground--get-lang-from-custom-or-default ()
  (or (hello-playground--get-lang-from-custom) 
      (hello-playground--get-lang-from-default)))

(defun hello-playground--get-lang-from-custom ()
  (hello-playground--generate-custom-lang-pair))

(defun hello-playground--get-lang-from-default ()
  (hello-playground--generate-default-lang-pair))

(defun hello-playground--generate-default-lang-pair ()
  (mapcar (lambda (dir) 
            (cons (f-filename dir) dir)) 
  	  (f-directories (hello-playground--default-env-path))))

(defun hello-playground--generate-custom-lang-pair ()
  (mapcar (lambda (dir) 
            (cons (f-filename dir) dir)) 
  	  (f-directories hello-playground-env-dir)))

(defun hello-playground--get-project-current ()
  (let ((var (project-current)))
    (cond 
     ((listp var) (car (last var)))
     (t var))))

(defun hello-playground--find-project-root (dir)
  (let ((root-dir (locate-dominating-file dir "hello-playground.json")))
    (when root-dir
      (list 'hello-playground root-dir))))

(add-hook 'project-find-functions #'hello-playground--find-project-root)

(defun hello-playground--list-playground-items (lang)    
  (let ((target-dir (f-join (f-expand hello-playground-dir) lang)))
    (when (f-directory-p target-dir)
      (mapcar #'f-filename
    	      (f-directories target-dir)))))

(defmacro hello-playground--with-playground (&rest body)
  `(let ((file-path (buffer-file-name)))
     (if (or (not file-path) (not (f-parent-of? hello-playground-dir file-path)))
         (error "current file is not in playground")
       ,body)))

(defun hello-playground--get-config-data (dir)
  (let ((config-file (f-join dir "hello-playground.json")))
    (if (not (f-exists-p config-file))
        (error "config file hello-playground.json not exists")

      (let-alist (json-read-file config-file)
        (if (member nil (list .executor .sourceFile .packageFile))
  	    (error "config file key not match")
  	  (list .executor .sourceFile .packageFile))))))

(provide 'hello-playground)
;;; hello-playground.el ends here
